#ifndef RythmPlayer_h
#define RythmPlayer_h

#include "Arduino.h"
#include "RythmPlayer.h"

class RythmPlayer
{
  public:
    RythmPlayer();
    boolean active;
    int s0, s1, s2, s3;
    void update();
    int ritmoDelay;
  private:
    int ritmoMax ;
    int ritmoIndex;
    String ritmo0;
    String ritmo1;
    String ritmo2;
    String ritmo3;
    int ritmo0Step;
    int ritmo1Step;
    int ritmo2Step;
    int ritmo3Step;
};

#endif
