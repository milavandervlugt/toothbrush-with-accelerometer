#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

/* This driver uses the Adafruit unified sensor library (Adafruit_Sensor),
   which provides a common 'type' for sensor data and some helper functions.

   To use this driver you will also need to download the Adafruit_Sensor
   library and include it in your libraries folder.

   You should also assign a unique ID to this sensor for use with
   the Adafruit Sensor API so that you can identify this particular
   sensor in any data logs, etc.  To assign a unique ID, simply
   provide an appropriate value in the constructor below (12345
   is used by default in this example).

   Connections
   ===========
   Connect SCL to analog 5 (or SCL on Arduino WIFI 1010 board)
   Connect SDA to analog 4 (or SCL on Arduino WIFI 1010 board)
   Connect VDD to 3-5V DC
   Connect GROUND to common ground

    messages begin sent:
  calibration system 0 gyro 3 accel 0 mag 0
  temperature 23
  eventtype 1 vector_accelerometer x -0.04 y -0.43 z 9.32
  eventtype 1 vector_linear_accelerometer x 0.02 y 0.04 z -0.46
  eventtype 1 vector_gravity x -0.03 y -0.46 z 9.79
  eventtype 2 vector_magnetometer x 4.87 y -7.06 z -67.06
  eventtype 3 vector_euler x 359.94 y -0.19 z 2.69
  eventtype 11 vector_gyroscope x -0.06 y 0.06 z -0.06


   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

/* Set the delay between fresh samples */
uint16_t BNO055_SAMPLERATE_DELAY_MS = 100;

Adafruit_BNO055 bno = Adafruit_BNO055(55);

void setup(void)
{
  Serial.begin(115200);
  Serial.println("Orientation Sensor Test"); Serial.println("");

  /* Initialise the sensor */
  if (!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while (1);
  }

  delay(1000);
}

void loop(void)
{
  
//  uint8_t system, gyro, accel, mag;
//  system = gyro = accel = mag = 0;
//  bno.getCalibration(&system, &gyro, &accel, &mag);
//  Serial.print("calibration ");
//  Serial.print("system ");
//  Serial.print(system);
//  Serial.print(" gyro ");
//  Serial.print(gyro);
//  Serial.print(" accel ");
//  Serial.print(accel);
//  Serial.print(" mag ");
//  Serial.print(mag);
//  Serial.println();


//  int8_t boardTemp = bno.getTemp();
//  Serial.print(F("temperature "));
//  Serial.println(boardTemp);

  // DONE: VECTOR_ACCELEROMETER
  // DONE: bno.getEvent(&linearAccelData, Adafruit_BNO055::VECTOR_LINEARACCEL);
  // DONE: VECTOR_GRAVITY
  // DONE: bno.getEvent(&orientationData, Adafruit_BNO055::VECTOR_EULER
  // DONE bno.getEvent(&angVelocityData, Adafruit_BNO055::VECTOR_GYROSCOPE);
  // DONE:VECTOR_MAGNETOMETER

//  // GIVES ACCELEROMETER DATA IN m/(sˆ2) (including all kind of wiggling motion etc..)
//  sensors_event_t vectorAccelerometer;
//  bno.getEvent(&vectorAccelerometer, Adafruit_BNO055::VECTOR_ACCELEROMETER);
//  printEvent(&vectorAccelerometer, "vector_accelerometer");

//  // GIVES GRAVITY ACCELEROMETER DATA IN m/(sˆ2) (usually one value is always 9.81 independent of wiggling)
//  sensors_event_t vectorGravity;
//  bno.getEvent(&vectorGravity, Adafruit_BNO055::VECTOR_GRAVITY);
//  printEvent(&vectorGravity, "vector_gravity");

//  // GIVES ACCELEROMETER DATA IN m/(sˆ2) WITHOUT THE EARTH'S FIELD (usually values are centered around 0)
//  sensors_event_t vectorLinearAccelerometer;
//  bno.getEvent(&vectorLinearAccelerometer, Adafruit_BNO055::VECTOR_LINEARACCEL);
//  printEvent(&vectorLinearAccelerometer, "vector_linear_accelerometer");


//  // something with the magnetic field...
//  sensors_event_t vectorMagnetometer;
//  bno.getEvent(&vectorMagnetometer, Adafruit_BNO055::VECTOR_MAGNETOMETER);
//  printEvent(&vectorMagnetometer, "vector_magnetometer");

//  // ORIENTATION OF THE DEVICE IN DEGREES (AS IN BUNNY EXAMPLE)
//  sensors_event_t vectorEuler;
//  bno.getEvent(&vectorEuler, Adafruit_BNO055::VECTOR_EULER);
//  printEvent(&vectorEuler, "vector_euler");

//  sensors_event_t event;
//  bno.getEvent(&event);
//  Serial.print(F("Orientation: "));
//  Serial.print((float)event.orientation.x);
//  Serial.print(F(" "));
//  Serial.print((float)event.orientation.y);
//  Serial.print(F(" "));
//  Serial.print((float)event.orientation.z);
//  Serial.println(F(""));
  
//
//  // GYROSCOPE. Looks like something related to angular velocity.
//  sensors_event_t vectorGyroscope;
//  bno.getEvent(&vectorGyroscope, Adafruit_BNO055::VECTOR_GYROSCOPE);
//  printEvent(&vectorGyroscope, "vector_gyroscope");





  delay(BNO055_SAMPLERATE_DELAY_MS);
}

void printEvent(sensors_event_t* event, String tag) {
  //Serial.println();
  Serial.print("eventtype ");
  Serial.print(event->type);
  Serial.print(" ");
  Serial.print(tag);
  double x = -1000000, y = -1000000 , z = -1000000; //dumb values, easy to spot problem
  if (event->type == SENSOR_TYPE_ACCELEROMETER) {
    x = event->acceleration.x;
    y = event->acceleration.y;
    z = event->acceleration.z;
  }
  else if (event->type == SENSOR_TYPE_ORIENTATION) {
    x = event->orientation.x;
    y = event->orientation.y;
    z = event->orientation.z;
  }
  else if (event->type == SENSOR_TYPE_MAGNETIC_FIELD) {
    x = event->magnetic.x;
    y = event->magnetic.y;
    z = event->magnetic.z;
  }
  else if ((event->type == SENSOR_TYPE_GYROSCOPE) || (event->type == SENSOR_TYPE_ROTATION_VECTOR)) {
    x = event->gyro.x;
    y = event->gyro.y;
    z = event->gyro.z;
  }

  Serial.print(" x ");
  Serial.print(x);
  Serial.print(" y ");
  Serial.print(y);
  Serial.print(" z ");
  Serial.println(z);
}

//
