import processing.serial.*;

boolean printSerial = false;
Serial port = null;

boolean SHOW_DATA = true;

BrushingGame game;

void setup()
{
  size(1150, 600, P3D);

  initSerial();
  initDataViz();
  
  game = new BrushingGame();
}

void keyPressed() {
  if (key==' ') SHOW_DATA = !SHOW_DATA;
}

void draw() {
  if (SHOW_DATA) {
    showData();
  } else {
    
    game.draw();
    
  }
}
